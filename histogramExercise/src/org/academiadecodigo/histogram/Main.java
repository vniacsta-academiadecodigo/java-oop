package org.academiadecodigo.histogram;

public class Main {

    public static final String STRING = "I am so tired so so so tired";

    public static void main(String[] args) {

//        HistogramComposition histogram = new HistogramComposition(STRING);
        HistogramInheritance histogram = new HistogramInheritance(STRING);

        System.out.println("My map has " + histogram.size() + " distinct words.");

        for (String word : histogram) {
            System.out.println(word + ": " + histogram.get(word));
        }
    }

}
