package org.academiadecodigo.histogram;

import java.util.HashMap;
import java.util.Iterator;

public class HistogramComposition implements Iterable<String> {

    // fields
    private HashMap<String, Integer> map;

    // constructor
    public HistogramComposition(String str) {
        map = new HashMap<>();
        String[] arrStr = str.split(" ");

        for (String word : arrStr) {

            if (map.containsKey(word)) {
                map.put(word, (map.get(word) + 1));
            } else {
                map.put(word, 1);
            }
        }
    }

    public int size() {
        return map.size();
    }

    public Integer get(String str) {
        return map.get(str);
    }

    @Override
    public Iterator<String> iterator() {
        return map.keySet().iterator();
    }
}
