package org.academiadecodigo.histogram;

import java.util.Iterator;
import java.util.HashMap;

public class HistogramInheritance extends HashMap<String, Integer> implements Iterable<String> {

    // constructor
    public HistogramInheritance(String str) {
        String[] arrStr = str.split(" ");

        for (String word : arrStr) {

            if (this.containsKey(word)) {
                super.put(word, (super.get(word) + 1));
            } else {
                super.put(word, 1);
            }
        }
    }

    @Override
    public Iterator<String> iterator() {
        return super.keySet().iterator();
    }


}
