package org.academiadecodigo.todoexercise;

public class ToDoItem implements Comparable<ToDoItem> {

    // fields
    private Importance importance;
    private int priority;
    private String item;

    // constructor
    public ToDoItem(Importance importance, int priority, String item) {
        this.importance = importance;
        this.priority = priority;
        this.item = item;
    }

    @Override
    public int compareTo(ToDoItem toDoItem) {

        if (importance.compareTo(toDoItem.importance) == 0) {
            return priority - toDoItem.priority;
        }
        return importance.compareTo(toDoItem.importance);
    }

    @Override
    public String toString() {
        return "ToDoItem{" +
                "importance = " + importance +
                ", priority = " + priority +
                ", item = '" + item + '\'' +
                '}';
    }
}
