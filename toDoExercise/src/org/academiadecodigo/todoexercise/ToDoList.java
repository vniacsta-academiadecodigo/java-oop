package org.academiadecodigo.todoexercise;

import java.util.PriorityQueue;

public class ToDoList {

    // fields
    private PriorityQueue<ToDoItem> priorityQueue;
    private ToDoItem toDoItem;
    private Importance importance;

    // constructor
    public ToDoList() {
        priorityQueue = new PriorityQueue<>();
    }

    public void add(Importance importance, int priority, String item) {
        priorityQueue.add(new ToDoItem(importance, priority, item));
    }

    public ToDoItem remove() {
        return priorityQueue.remove();
    }

    public boolean isEmpty() {
        return priorityQueue.isEmpty();
    }

}
