package org.academiadecodigo.todoexercise;

public enum Importance {

    HIGH,
    MEDIUM,
    LOW
}
