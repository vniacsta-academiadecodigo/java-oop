package org.academiadecodigo.asynctomatics.exceptionsExercise;

public class File {

    // properties
    private String name;

    // constructor
    public File(String name) {
        this.name = name;
    }

    // getter
    public String getName() {
        return name;
    }
}
