package org.academiadecodigo.asynctomatics.exceptionsExercise;

import org.academiadecodigo.asynctomatics.exceptionsExercise.exceptions.FileNotFoundException;
import org.academiadecodigo.asynctomatics.exceptionsExercise.exceptions.NotEnoughPermissionsException;
import org.academiadecodigo.asynctomatics.exceptionsExercise.exceptions.NotEnoughSpaceException;

public class Main {

    public static void main(String[] args) {

        capacityTest();
        invalidFileTest();
    }

    private static void capacityTest() {

        FileManager fileManager = new FileManager(3);

        try {
            fileManager.createFile("Desk");
            fileManager.createFile("Chair");
            fileManager.createFile("Table");
            fileManager.createFile("Sofa");

        } catch (NotEnoughSpaceException ex) {
            System.out.println(ex.getMessage());

        } finally {
            System.out.println("Capacity test \n");
        }
    }

    private static void invalidFileTest() {

        FileManager fileManager = new FileManager(3);

        try {
            fileManager.login();
            fileManager.getFile("Desk");
            fileManager.getFile("Bed");

        } catch (FileNotFoundException ex) {
            System.out.println(ex.getMessage());

        } catch (NotEnoughPermissionsException ex) {
            System.out.println(ex.getMessage());

        } finally {
            fileManager.logout();
            System.out.println("Logging out");
        }
    }
}
