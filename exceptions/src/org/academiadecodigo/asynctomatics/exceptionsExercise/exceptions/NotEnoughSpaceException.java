package org.academiadecodigo.asynctomatics.exceptionsExercise.exceptions;

public class NotEnoughSpaceException extends FileException {

    public NotEnoughSpaceException() {
        super("Not enough space in array");
    }
}
