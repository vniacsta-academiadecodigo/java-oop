package org.academiadecodigo.asynctomatics.exceptionsExercise.exceptions;

public class FileNotFoundException extends FileException {

    public FileNotFoundException() {
        super("File not found");
    }
}
