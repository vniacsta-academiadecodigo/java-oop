package org.academiadecodigo.asynctomatics.exceptionsExercise.exceptions;

public class NotEnoughPermissionsException extends FileException {

    public NotEnoughPermissionsException() {
        super("Not logged in");
    }
}
