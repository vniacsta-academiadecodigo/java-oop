package org.academiadecodigo.asynctomatics.exceptionsExercise.exceptions;

public class FileException extends Exception {

    public FileException(String message) {
        super(message);
    }
}
