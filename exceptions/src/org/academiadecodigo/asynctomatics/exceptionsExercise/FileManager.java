package org.academiadecodigo.asynctomatics.exceptionsExercise;

import org.academiadecodigo.asynctomatics.exceptionsExercise.exceptions.FileNotFoundException;
import org.academiadecodigo.asynctomatics.exceptionsExercise.exceptions.NotEnoughPermissionsException;
import org.academiadecodigo.asynctomatics.exceptionsExercise.exceptions.NotEnoughSpaceException;

public class FileManager {

    // properties
    private boolean loggedIn;
    private File[] files;

    // constructor
    public FileManager(int maxNumFiles) {
        this.loggedIn = false;
        this.files = new File[maxNumFiles];
    }

    // method to get files
    public File getFile(String name) throws NotEnoughPermissionsException, FileNotFoundException {

        // if it's not logged in, throw exception
        if (!loggedIn) {
            throw new NotEnoughPermissionsException();
        }

        // iterate to check if there is a file with the provided name
        for (int i = 0; i < files.length; i++) {

            if (files[i] == null) {
                break;
            }

            if (name.equals(files[i].getName())) {
                return files[i];
            }
        }
        // if there isn't a match, throw exception
        throw new FileNotFoundException();
    }

    // method to create files
    public void createFile(String name) throws NotEnoughSpaceException {

        // iterate to check if there is still space to create a file in our array
        for (int i = 0; i < files.length; i++) {

            if (files[i] == null) {
                files[i] = new File(name);
                return;
            }
        }
        // if all indexes are not null, throw exception
        throw new NotEnoughSpaceException();
    }

    // method to login
    public void login() {
        loggedIn = true;
    }

    // method to logout
    public void logout() {
        loggedIn = false;
    }
}
