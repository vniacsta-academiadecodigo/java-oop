package org.academiadecodigo.asynctomatics.range;

import java.util.Iterator;

public class RangeTest {

    public static void main(String[] args) {

        Range range = new Range(-5, 5);

        System.out.println("--- USING ITERATOR ---");

//        Iterator<Integer> it = range.iterator();
//
//        while (it.hasNext()) {
//            System.out.println("Iterated: " + it.next());
//        }

        for (Integer i : range) {
            System.out.println("Iterated: " + i);
        }
    }
}
