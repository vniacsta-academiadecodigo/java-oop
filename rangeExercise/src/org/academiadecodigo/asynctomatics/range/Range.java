package org.academiadecodigo.asynctomatics.range;

import java.util.Iterator;

public class Range implements Iterable<Integer> {

    // fields
    private int start;
    private int end;

    // constructor
    public Range(int start, int end) {
        this.start = start;
        this.end = end;
    }

    // method from
    @Override
    public Iterator<Integer> iterator() {
        return new RangeIterator(start, end);
    }

    // new private class
    // E will allow to type later on
    private class RangeIterator<E> implements Iterator<Integer> {

        // fields
        private int start;
        private int end;

        // constructor
        public RangeIterator(int start, int end) {
            this.start = start;
            this.end = end;
        }

        @Override
        public boolean hasNext() {
            return start <= end;
        }

        @Override
        public Integer next() {
            return start++;
        }
    }

}
