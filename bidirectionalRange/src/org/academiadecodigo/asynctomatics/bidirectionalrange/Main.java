package org.academiadecodigo.asynctomatics.bidirectionalrange;

import java.util.Iterator;

public class Main {

    public static void main(String[] args) {

        BidirectionalRange biRange = new BidirectionalRange(3, 9);
        biRange.setReversed(false);

        System.out.println(" *** USING FORWARD ITERATOR *** ");
        Iterator<Integer> iterator = biRange.iterator();

        while (iterator.hasNext()) {

            int i = iterator.next();
            System.out.println(i);

            if (i == 8) {
                System.out.println("Removed " + i);
                iterator.remove();
            }
        }

        System.out.println(" *** FORWARD ITERATOR WITHOUT NUMBER 8 *** ");
        iterator = biRange.iterator();

        while (iterator.hasNext()) {
            System.out.println(iterator.next());

            // reverse the iterator while iterating
            // SHOULDN'T WORK IN THIS LOOP, ONLY ON THE NEXT LOOP
            biRange.setReversed(true);
        }

        System.out.println(" *** RESERVED ITERATOR *** ");
        iterator = biRange.iterator();

        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }
    }
}
