package org.academiadecodigo.asynctomatics.bidirectionalrange;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.NoSuchElementException;

public class BidirectionalRange implements Iterable<Integer> {

    // fields
    private int minRange;
    private int maxRange;
    private boolean reversed;
    private List<Integer> removed;

    // constructor
    public BidirectionalRange(int minRange, int maxRange) {
        this.minRange = minRange;
        this.maxRange = maxRange;
        this.reversed = false;
        removed = new LinkedList<>();
    }

    // setter
    public void setReversed(boolean reversed) {
        this.reversed = reversed;
    }

    @Override
    public Iterator<Integer> iterator() {

        return !reversed ? ascendingIterator() : descendingIterator();
    }

    private Iterator<Integer> ascendingIterator() {
        return new Iterator<Integer>() {

            private int current = minRange - 1;

            @Override
            public boolean hasNext() {
                while (removed.contains(current + 1)) {
                    current++;
                }
                return current < maxRange;
            }

            @Override
            public Integer next() {
                if (!hasNext()) {
                    throw new NoSuchElementException();
                }
                return current++;
            }

            @Override
            public void remove() {
                if (removed.contains(current)) {
                    throw new IllegalStateException("You cannot call remove() repeatedly");
                }
                removed.add(current);
            }
        };
    }

    private Iterator<Integer> descendingIterator() {
        return new Iterator<Integer>() {

            private int current = maxRange + 1;

            @Override
            public boolean hasNext() {
                while (removed.contains(current - 1)) {
                    current--;
                }
                return current > minRange;
            }

            @Override
            public Integer next() {
                if (!hasNext()) {
                    throw new NoSuchElementException();
                }
                return current--;
            }

            @Override
            public void remove() {
                if (removed.contains(current)) {
                    throw new IllegalStateException("You cannot call remove() repeatedly");
                }
                removed.add(current);
            }
        };
    }
}
