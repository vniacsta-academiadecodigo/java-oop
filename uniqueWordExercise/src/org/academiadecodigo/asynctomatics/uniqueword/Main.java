package org.academiadecodigo.asynctomatics.uniqueword;

public class Main {

    public static final String STRING = "vania vania focus on your your exercise";

    public static void main(String[] args) {

        UniqueWord uniqueWord = new UniqueWord(STRING);

        for (String word : uniqueWord) {
            System.out.println(word);
        }
    }
}
