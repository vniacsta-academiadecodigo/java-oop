package org.academiadecodigo.asynctomatics.uniqueword;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;

public class UniqueWord implements Iterable<String> {

    private HashSet<String> set;

    // constructor
    public UniqueWord(String str) {
        String[] arrStr = str.split(" ");
        set = new HashSet<>();

        set.addAll(Arrays.asList(arrStr));
//        for (String word : arrStr) {
//            set.add(word);
//        }
    }

    @Override
    public Iterator<String> iterator() {
        return set.iterator();
    }
}
