package org.academiadecodigo.bootcamp56.calculator;

public class Calculator {

    // creating my Calculator properties - private
    private String brand;
    private String color;

    // creating my constructor method
    public Calculator(String brand, String color) {
        this.brand = brand;
        this.color = color;
    }

    // creating my methods
    public int add(int num1, int num2) {
        return num1 + num2;
    }

    public int subtract(int num1, int num2) {
        return num1 - num2;
    }

    public int multiply(int num1, int num2) {
        return num1 * num2;
    }

    public double divide(int num1, int num2) {
        return (double) num1 / num2;
    }
}
