package org.academiadecodigo.asynctomatics.calculator;

public enum Operation {

    ADD,
    SUBTRACT,
    MULTIPLY,
    DIVIDE
}
